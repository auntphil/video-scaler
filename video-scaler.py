import os
import subprocess
import datetime

videoPath = "D:/Movies"
specialFeatures = [
	"behind the scenes",
	"deleted scenes",
	"featurettes",
	"interviews",
	"scenes",
	"shorts",
	"trailers",
	"other",
]

def writeLog(data):
	f = open("D:/VideoScaler/logs/runtime.log","a+")
	f.write(data + "\r")
	f.close()

#Check if time to stop
#checking if loop should stop
#Script is killed after specified time
def checkTimeStop():
	if datetime.datetime.now().hour > 6:
		now = datetime.datetime.now()
		writeLog("Stopped @ %s:%s" % (now.hour, f"{now.minute:02d}") )
		writeLog("---")
		exit()

#Checks if Special Feature Folder
def checkFolderName( dir ):
	if str.lower(dir) in specialFeatures:
		return True
	else:
		return False
		
#Converts video and creates complete marker
def convertVideo( path, video ):
	original = path + '/' + video
	temp = 'D:/temp/' + video
		
	#converting Video
	subprocess.call('c:/ffmpeg/bin/ffmpeg -i "%s" -s hd720 -c:v libx265 -preset medium -crf 28 -c:a aac -b:a 128k "%s"' % (original, temp))
	
	#Deleting and Renaming
	os.remove(original)
	os.rename(temp, original)
	
	now = datetime.datetime.now()
	writeLog("Converted: %s @ %s:%s" % (original, now.hour, f"{now.minute:02d}") )
	
	#marking video as converted
	open(original + '.complete', 'a').close()

def walkFolder(path, step, folder):
	for (dirpath, dirnames, filenames) in os.walk(path):
		
		#Checking if in a Special Features Folder
		if checkFolderName( folder ):
			for video in filenames:
				#Checking Time
				checkTimeStop()
				#Checks if the video has already been converted
				if video + '.complete' in filenames:
					pass
				else:
					if video.endswith( ('.mkv', '.mp4') ):
						convertVideo( dirpath, video )
	
		#Checking if no children folders
		if len(dirnames) != 0:
			for dir in dirnames:
				
				#Checking if Main Movie folder or if directory is a Special Feature Folder
				if step == 0 or checkFolderName(dir):
					walkFolder( dirpath + '/' + dir, step+1, dir)

def main():
	now = datetime.datetime.now()
	writeLog("Started on %s %s, %s @ %s:%s" % (now.strftime("%B"), now.day, now.year, f"{now.hour:02d}", f"{now.minute:02d}") )
	walkFolder(videoPath, 0, "")
	now = datetime.datetime.now()
	writeLog("Finished @ %s:%s" % (now.hour, f"{now.minute:02d}") )
	writeLog("---")
	
main()